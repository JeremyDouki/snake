let canv 
let ctx
let x = 0
let y = 0
let longueur
let hauteur
let snake= new Array()
let walls= new Array()
let food= new Array()
let score=0
let tail = 3
let delai
let tete
let taille
let boost = new Array()

function start() {
    let obj= JSON.parse(taille)
    canv = document.getElementById("game")
    ctx=canv.getContext("2d")
    document.addEventListener("keydown",act)
    longueur=hauteur=obj.dimensions
    x = 0
    y = 1
    tete=obj.Snake[0]
    snake= [obj.Snake[1],obj.Snake[2]]
    delai = obj.delay
    walls = obj.wall
    food= obj.food
    setInterval(step,delai)
}



function step()
{
    
    tete = [tete[0]+x,tete[1]+y]
    if (tete[0]<0)
    {
        tete[0]=longueur-1
    }
    if (tete[0]>longueur-1)
    {
        tete[0]=0
    }
    if (tete[1]<0)
    {
        tete[1]=hauteur-1
    }
    if (tete[1]>hauteur-1)
    {
        tete[1]=0
    }    
    ctx.fillStyle="black"
    ctx.fillRect(0,0,canv.width,canv.height)
    ctx.strokeStyle="white"
    ctx.strokeText("score: "+score,canv.width-50,10)
    ctx.fillStyle="red"
    ctx.fillRect(food[0]*canv.width/longueur,food[1]*canv.width/longueur,canv.width/longueur-2,canv.width/longueur-2)
    ctx.fillStyle="blue"
    ctx.fillRect(boost[0]*canv.width/longueur,boost[1]*canv.width/longueur,canv.width/longueur-2,canv.width/longueur-2)
    ctx.fillStyle="orange"
    for(let i=0; i<walls.length;i++)
    {
        ctx.fillRect(walls[i][0]*canv.width/longueur,walls[i][1]*canv.width/longueur,canv.width/longueur,canv.width/longueur)
        if(walls[i][0]==tete[0] && walls[i][1]==tete[1])
        {
            tail=3
            score=0 
        }
    }
    ctx.fillStyle="lime"
    for(let i=0; i<snake.length;i++)
    {
        ctx.fillRect(snake[i][0]*canv.width/longueur,snake[i][1]*canv.width/longueur,canv.width/longueur-2,canv.width/longueur-2)
        if(snake[i][0]==tete[0] && snake[i][1]==tete[1])
        {
            tail=3
            score=0
            console.log("game over")
        }
    }
    
   
    if(tete[0]==food[0]&&tete[1]==food[1])
    {
        tail++
        score++
        for(let i =0;i<walls.length;i++)
        {
            do
            {
                food[0]=Math.floor(Math.random()*longueur)
                food[1]=Math.floor(Math.random()*longueur)
            }while(food[0]==walls[i][0]&&food[1]==walls[i][1])
        }
    }
    snake.push([tete[0],tete[1]])
    while(snake.length>tail)
    {
        snake.shift()
    }
    if(Math.floor(Math.random()*1000)<6)
    {
        for(let i =0;i<walls.length;i++)
        {
            do
            {
                boost[0]=Math.floor(Math.random()*longueur)
                boost[1]=Math.floor(Math.random()*longueur)
            }while(boost[0]==walls[i][0]&&boost[1]==walls[i][1]||boost[0]==food[0]&&boost[1]==food[1])
        }
    }
    if(tete[0]==boost[0]&&tete[1]==boost[1])
    {
        score+=2
        boost.shift()
    }
    
}

function act (evt)
{
    switch(evt.keyCode)
    {
        case 37 :
            x=-1
            y=0
            console.log("tete")
            break
        case 38 :
            x=0
            y=-1
            console.log("up")
            break
        case 39 :
            x=1
            y=0
            console.log("droite")
            break
        case 40 :
            x=0
            y=1
            console.log("bas")
            break
    }
}
 function grilleP()
 {
    taille = '{ "dimensions":20,"delay":200, "wall":[[5,5], [5,6], [5,7], [5,8], [7, 15], [7, 14], [7, 13]],"food":[10,10],"Snake": [[6,6],[6,5],[6,4]]}'
    start()
 }

 function grilleM()
 {
    taille = '{ "dimensions":30,"delay":150, "wall":[[5,5], [5,6], [5,7], [5,8], [24, 15], [25, 14], [26, 13]],"food":[10,10],"Snake": [[15,15],[15,16],[15,17]]}'
    start()
 }

 function grilleG()
 {
    taille = '{ "dimensions":40,"delay":100, "wall":[[5,5], [5,6], [5,7], [5,8], [24, 15], [25, 14], [26, 13],[30,30],[30,31],[30,32]],"food":[10,10],"Snake": [[20,15],[20,16],[20,17]]}'
    start()
 }